﻿using UnityEngine;
using System.Collections;

public class SelfDestroyer : MonoBehaviour 
{
	private GameObject mainCamera;
	public float deleteDistance = 5;

	void Start()
	{
		mainCamera = GameObject.Find ("Main Camera");
	}

	void Update () 
	{
		if (mainCamera == null)
			return;
		
		Vector3 cameraPos = mainCamera.transform.position;
		Vector3 objectPos = transform.position;

		if (cameraPos.z - objectPos.z - GetComponentInChildren<Renderer> ().bounds.size.z / 2 > deleteDistance)
		{
			Destroy (gameObject);
		}
	}
}
