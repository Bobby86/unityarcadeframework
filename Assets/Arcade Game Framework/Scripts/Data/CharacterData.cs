﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


[CreateAssetMenu (menuName = "Character", fileName = "Assets/Resources/CharactersData/Character000")]
public class CharacterData :ScriptableObject 
{
	public new string name = "";
	public GameObject characterObject;
	public Sprite image;
	public int price = 100;
	public bool locked = true;		// true - locked (not purchased); flase - unlocked (purchased)
}
