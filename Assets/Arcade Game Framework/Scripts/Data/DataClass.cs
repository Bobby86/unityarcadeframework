﻿using UnityEngine;
using System.Collections;


public class DataClass : MonoBehaviour 
{
	public static DataClass Instance;

	private UICharacterController currentlySelectedCharacter = null;
	private CharacterData[] charactersData;


	void Awake ()
	{
		Instance = this;
		charactersData = Resources.LoadAll <CharacterData> ("CharactersData/");

		if (!PlayerPrefs.HasKey ("currentCharacterIndex"))
		{
			PlayerPrefs.SetInt ("currentCharacterIndex", 0);
			PlayerPrefs.Save ();
		}
	}

	public CharacterData[] getCharactersData ()
	{
		return charactersData;
	}

	public bool checkAvailableCharacterForUnlock (int playerCoinsAmount)
	{
		foreach (CharacterData character in charactersData)
		{
			if (character.locked == true && character.price <= playerCoinsAmount)
			{
				return true;
			}
		}

		return false;
	}

	public int getCurrentCharacterIndex ()
	{
		return PlayerPrefs.GetInt ("currentCharacterIndex");
	}

	public UICharacterController getSelectedCharacterInMenu ()
	{
		return currentlySelectedCharacter;
	}

	public CharacterData getCurrentCharacter ()
	{
		return charactersData [PlayerPrefs.GetInt ("currentCharacterIndex")];
	}

	public CharacterData getCharacterDataByIndex (int index)
	{
		if (index < 0 || index >= charactersData.Length)
		{
			return null;
		}

		return charactersData [index];
	}

	public void setCurrentCharacterFromMenu (int index, UICharacterController characterInstance = null)
	{
		if (characterInstance)
		{
			currentlySelectedCharacter = characterInstance;
		}

		if (index == PlayerPrefs.GetInt ("currentCharacterIndex"))
		{
			GameManager.Instance.setSelectedCharacter ();
			return;
		}

		PlayerPrefs.SetInt ("currentCharacterIndex", index);
		PlayerPrefs.Save ();
		GameManager.Instance.setSelectedCharacter ();
	}
}
