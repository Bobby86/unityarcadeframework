﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerData 
{
	public bool adsEnabled;
	public int bestScore;
	public int totalCoins;
	public float soundVolume = 1;
}