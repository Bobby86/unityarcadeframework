﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

public class AdManager : Singleton<AdManager>
{
	public GameObject removeAdsButton;

	[SerializeField] bool disableAds = false;
	[Tooltip ("X: every X gameover an Ad would be shown")]
	[SerializeField] int showAdEachXGameOver = 1;
	[Tooltip ("check Ad readiness with this rate(time), if ad not ready or initialized yet")]
	[SerializeField] float checkRateIfAdReady = 0.5f;
	[SerializeField] string androidGameID;
	[SerializeField] string iosGameID;

	void OnEnable()
	{
		GameManager.onGameOverEvent += onGameOverEvent;
	}

	void OnDisable()
	{
		GameManager.onGameOverEvent += onGameOverEvent;
	}

	public void Initialize ()
	{
		#if UNITY_ADS
		if (!Advertisement.isSupported)
		{
			disableAds = true;
			turnOffAds ();
		}

		if (!disableAds)
		{
			#if UNITY_IOS
			Advertisement.Initialize (iosGameID, enableTestMode);
			#elif UNITY_ANDROID
			Advertisement.Initialize (androidGameID, true);
			#endif
			
			if (removeAdsButton != null)
			{
				removeAdsButton.SetActive (true);
			}
		}
		#else
		turnOffAds ();
		#endif
	}

	public void turnOffAds ()
	{
		if (removeAdsButton != null && removeAdsButton.activeSelf)
		{
			removeAdsButton.SetActive (false);
		}

		disableAds = true;
		this.enabled = false;
	}

	private void onGameOverEvent ()
	{
		if (disableAds)
		{
			return;
		}

		if (showAdEachXGameOver > 0 && (GameManager.Instance.getGameOverCount () % showAdEachXGameOver == 0))
		{
			if (checkRateIfAdReady <= 0)
			{
				checkRateIfAdReady = 0.5f;
			}

			StartCoroutine (showMyAd (checkRateIfAdReady));
		}
	}

	private IEnumerator showMyAd (float delay)
	{
		#if UNITY_ADS
		while (!Advertisement.isInitialized || !Advertisement.IsReady ())
		{
			yield return new WaitForSeconds (delay);
		}

		Advertisement.Show ();
		#else
		yield return null;
		#endif
	}
}
