﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class GridParameters : MonoBehaviour 
{
	public float maxCellWidth = 100f;
	public float minSpacing = 3f;

	private RectTransform thisRT;
	private GridLayoutGroup gridLayout;


	void Start ()
	{
		if (thisRT == null)
		{
			thisRT = GetComponent <RectTransform> ();
		}

		if (gridLayout == null)
		{
			gridLayout = GetComponent <GridLayoutGroup> ();
		}

		if (transform.childCount == 0)
		{
			setCellParametersToGridView ();
		}
	}


	private void setCellParametersToGridView ()
	{
		float cellsWidth = maxCellWidth * gridLayout.constraintCount + minSpacing * (gridLayout.constraintCount - 1);

		if (cellsWidth <= ScrollViewParameters.ScrollViewWidth)
		{
			float spacing = (ScrollViewParameters.ScrollViewWidth - cellsWidth) / gridLayout.constraintCount;
			gridLayout.cellSize = new Vector2 (maxCellWidth, maxCellWidth + 0.2f * maxCellWidth);
			gridLayout.spacing = new Vector2 (spacing, spacing);
			thisRT.offsetMin = new Vector2 (spacing, thisRT.offsetMin.y);
			thisRT.offsetMax = new Vector2 (thisRT.offsetMax.x, -spacing / 2);
			GetComponent <UICharactersMenuController> ().charactersSpawn (spacing);
		}
		else
		{
			float singleCellWidth = (ScrollViewParameters.ScrollViewWidth - gridLayout.constraintCount * minSpacing) / gridLayout.constraintCount;
			gridLayout.cellSize = new Vector2 (singleCellWidth, singleCellWidth + 0.2f * singleCellWidth);
			gridLayout.spacing = new Vector2 (minSpacing, minSpacing);
			thisRT.offsetMin = new Vector2 (minSpacing / 2, thisRT.offsetMin.y);
			thisRT.offsetMax = new Vector2 (thisRT.offsetMax.x, -minSpacing / 2);
			GetComponent <UICharactersMenuController> ().charactersSpawn (minSpacing);
		}
	}
}
