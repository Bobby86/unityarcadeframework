﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class UICharacterController : MonoBehaviour 
{
	public int characterIndex;	// index of this character in CharacterData[] charactersData
	public Text priceText;
	public Text nameText;
	public Color purchasedColor;
	public Color borderColor;
	public Color selectedColor;

	private bool unlocked = false;
	private Image buttonImage;
	private CharacterData characterData;

	public delegate void Event ();
	public static event Event onCharacterSelect;


	void Awake ()
	{
		checkComponentsForNull ();
	}

	public void initialize (int index, CharacterData character)
	{
		priceText.text = character.price.ToString ();
		nameText.text = character.name.ToUpper ();
		unlocked = !character.locked;
		characterData = character;
		characterIndex = index;

		if (unlocked)
		{
			setUnlocked ();
		}
	}

	public void setSelected ()
	{
		UICharacterController selectedCharacter = DataClass.Instance.getSelectedCharacterInMenu ();

		if (this != selectedCharacter)
		{
			if (unlocked)
			{
				checkComponentsForNull ();

				if (selectedCharacter != null)
				{
					selectedCharacter.setUnselected ();
				}

				DataClass.Instance.setCurrentCharacterFromMenu (characterIndex, this);
				buttonImage.color = selectedColor;

				if (onCharacterSelect != null)
				{
					onCharacterSelect ();
				}
			}
			else
			{
				if (GameManager.Instance.makePurchase (characterData.price))
				{
					ConfirmationBoxController.ConfirmationBoxDelegate confirmUnlock = setUnlocked;
					ConfirmationBoxController.Instance.setMessage ("Unlock Character", "Do you want to unlock this character?", confirmUnlock);
				}
				else
				{
					MessageBoxController.Instance.setMessage ("Insufficient coin", "You do not have enough coin to unlock this character.");
				}
			}
		}
	}

	public void setUnselected ()
	{
		if (unlocked)
		{
			checkComponentsForNull ();
			buttonImage.color = purchasedColor;
		}
	}

	public void setUnlocked ()
	{
		unlocked = true;
		characterData.locked = false;

		checkComponentsForNull ();
		buttonImage.color = purchasedColor;
		buttonImage.transform.parent.GetComponent <Image> ().color = borderColor;
		setSelected ();
	}

	public bool isLocked ()
	{
		return characterData.locked;
	}

	private void checkComponentsForNull ()
	{
		if (buttonImage == null)
		{
			Button button = GetComponentInChildren <Button> ();

			if (button != null)
			{
				buttonImage = button.GetComponent <Image> ();
			}
		}
	}
}
