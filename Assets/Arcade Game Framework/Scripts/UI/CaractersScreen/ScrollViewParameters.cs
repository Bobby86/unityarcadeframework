﻿using UnityEngine;
using System.Collections;

public class ScrollViewParameters : MonoBehaviour 
{
	public static float ScrollViewWidth;

	private RectTransform thisRT;


	void Awake () 
	{
		thisRT = GetComponent <RectTransform> ();
		ScrollViewWidth = thisRT.rect.width;
	}

}
