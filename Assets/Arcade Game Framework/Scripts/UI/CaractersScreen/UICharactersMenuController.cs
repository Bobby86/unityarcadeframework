﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class UICharactersMenuController : MonoBehaviour
{
	public static UICharactersMenuController Instance;

	public RectTransform content;
	public GameObject characterPrefab;

	private List<UICharacterController> spawnedCharacters = new List<UICharacterController> ();

	void Awake ()
	{
		Instance = this;
	}

	public void charactersSpawn (float spacing)
	{
		int charCount = 0;
		int currentCharIndex = PlayerPrefs.GetInt ("currentCharacterIndex");
		GameObject charGO = null;

		foreach (CharacterData character in DataClass.Instance.getCharactersData ())
		{
//			if (character.characterObject != null)
			if (character.name != "")	// tests
			{
				charGO = Instantiate (characterPrefab) as GameObject;
				charGO.transform.SetParent (transform, false);
				spawnedCharacters.Add (charGO.GetComponent <UICharacterController> ());
				spawnedCharacters[charCount].initialize (charCount, character);
				++charCount;
			}
		}

		if (spawnedCharacters.Count > currentCharIndex)
		{			
			if (spawnedCharacters[currentCharIndex].isLocked ())
			{
				spawnedCharacters[currentCharIndex].setUnlocked ();
			}
			else
			{
				spawnedCharacters[currentCharIndex].setSelected ();
			}
			DataClass.Instance.setCurrentCharacterFromMenu (currentCharIndex, spawnedCharacters[currentCharIndex]);
		}
		else
		{		
			if (spawnedCharacters[0].isLocked ())
			{
				spawnedCharacters[0].setUnlocked ();
			}
			else
			{
				spawnedCharacters[0].setSelected ();
			}
			DataClass.Instance.setCurrentCharacterFromMenu (currentCharIndex, spawnedCharacters[0]);
		}

		if (charGO != null)
		{
			StartCoroutine (setContentBottom (spacing, 0.01f));
		}
	}


	private IEnumerator setContentBottom (float spacing, float delay)
	{
		yield return new WaitForSeconds (delay);

		RectTransform lastCharRT = spawnedCharacters[spawnedCharacters.Count - 1].GetComponent <RectTransform> ();
		content.sizeDelta = new Vector2 (content.sizeDelta.x, Mathf.Abs (lastCharRT.anchoredPosition.y) + spacing + (lastCharRT.rect.height / 2));
		spawnedCharacters[DataClass.Instance.getCurrentCharacterIndex ()].setSelected ();
	}
}
