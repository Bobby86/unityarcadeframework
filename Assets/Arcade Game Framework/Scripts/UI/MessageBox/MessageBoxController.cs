﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class MessageBoxController : Singleton<MessageBoxController>
{
	public Text titleText;
	public Text messageText;
	public GameObject messageBox;

	private bool messageBoxIsActive = false;

	public void Start ()
	{
		turnOffMessageBox ();
	}

	public void setMessage (string title = "", string message = "")
	{
		if (!messageBoxIsActive)
		{
			titleText.text = title.ToUpper ();
			messageText.text = message.ToUpper ();
			turnOnMessageBox ();
		}
	}

	public void okButtonClick ()
	{
		turnOffMessageBox ();
	}

	private void turnOnMessageBox ()
	{
		if (!messageBox.activeSelf)
		{
			messageBoxIsActive = true;
			messageBox.SetActive (true);
		}
	}

	private void turnOffMessageBox ()
	{
		if (messageBox.activeSelf)
		{
			messageBoxIsActive = false;
			messageBox.SetActive (false);
		}
	}
}
