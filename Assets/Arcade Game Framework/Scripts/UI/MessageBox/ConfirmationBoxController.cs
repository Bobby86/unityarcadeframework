﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class ConfirmationBoxController : Singleton<ConfirmationBoxController>
{	
	public delegate void ConfirmationBoxDelegate ();
	private ConfirmationBoxDelegate callbackConfirmation;

	public Text titleText;
	public Text messageText;
	public GameObject messageBox;

	private bool messageBoxIsActive = false;

	public void setMessage (string title = "", string message = "", ConfirmationBoxDelegate  callBack = null)
	{
		if (!messageBoxIsActive)
		{
			titleText.text = title.ToUpper ();
			messageText.text = message.ToUpper ();
			turnOnMessageBox ();
			callbackConfirmation = callBack;
		}
	}

	public void okButtonClick ()
	{
		if (callbackConfirmation != null)
		{
			callbackConfirmation ();
		}

		turnOffMessageBox ();
	}

	public void cancelButtonClick ()
	{
		turnOffMessageBox ();
	}

	private void turnOnMessageBox ()
	{
		if (!messageBox.activeSelf)
		{
			messageBoxIsActive = true;
			messageBox.SetActive (true);
		}
	}

	private void turnOffMessageBox ()
	{
		if (messageBox.activeSelf)
		{
			messageBoxIsActive = false;
			messageBox.SetActive (false);
		}
	}
}
