﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour 
{

	// The target we are following
	[SerializeField]
	private Transform target;

	[SerializeField]
	private float distance = 5;

	void Start () 
	{
	}

	private void LateUpdate()
	{
		if (target == null)
			return;

		Vector3 position = transform.position;
		position.z = target.position.z - distance;
		transform.position = position;
	}
}
