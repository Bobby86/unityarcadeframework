﻿using UnityEngine;
using System.Collections;

public class CoinAnimation : MonoBehaviour {

	[SerializeField]
	private float rotateSpeed = 1.0f;

	[SerializeField]
	private float floatSpeed = 1.0f;

	[SerializeField]
	private float floatDistance = 1.0f;

	private bool isMoveingUp = true;
	private float startY = 0f;

	// Use this for initialization
	void Start () {
		startY = transform.position.y;
		//floatSpeed = Random.Range (0.5f, 1f);
		//transform.Rotate( transform.up, Random.Range(0f, 360f));
		StartCoroutine (spin ());
		StartCoroutine (floating ());
	}
	
	private IEnumerator spin()
	{
		while(true)
		{
			transform.Rotate( transform.up, 360 * rotateSpeed * Time.deltaTime);
			yield return 0;
		}
	}

	private IEnumerator floating()
	{
		while (true) 
		{
			float newY = transform.position.y + (isMoveingUp ? 1 : -1) * 2 * floatSpeed * Time.deltaTime;

			if (newY > startY + floatDistance) {
				newY = startY + floatDistance;
				isMoveingUp = false;
			} else if (newY < startY) {
				newY = startY;
				isMoveingUp = true;
			}

			transform.position = new Vector3 (transform.position.x, newY, transform.position.z);
			yield return 0;
		}
	}
}
