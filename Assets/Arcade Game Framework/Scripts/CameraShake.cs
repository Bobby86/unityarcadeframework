﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

	public float duration = 2f;
	public float speed = 20f;
	public float magnitude = 2f;
	public AnimationCurve damper = new AnimationCurve(new Keyframe(0f, 1f), new Keyframe(0.9f, .33f, -2f, -2f), new Keyframe(1f, 0f, -5.65f, -5.65f));

	private Vector3 originalPosition;

	/* Events */
	public delegate void Event();

	void OnEnable()
	{
		originalPosition = transform.position;
		GameManager.onGameOverEvent += onGameOverEvent;
	}

	void OnDisable()
	{
		GameManager.onGameOverEvent -= onGameOverEvent;
	}

	public void onGameOverEvent() 
	{
		StartCoroutine(Shake(transform, originalPosition, duration, speed, magnitude, damper));
	}


	IEnumerator Shake(Transform transform, Vector3 originalPosition, float duration, float speed, float magnitude, AnimationCurve damper = null)
	{
		float elapsed = 0f;
		while (elapsed < duration) 
		{
			elapsed += Time.deltaTime;			
			float damperedMag = (damper != null) ? (damper.Evaluate(elapsed / duration) * magnitude) : magnitude;
			float x = (Mathf.PerlinNoise(Time.time * speed, 0f) * damperedMag) - (damperedMag / 2f);
			float y = (Mathf.PerlinNoise(0f, Time.time * speed) * damperedMag) - (damperedMag / 2f);
			transform.localPosition = new Vector3(originalPosition.x + x, originalPosition.y + y, originalPosition.z);
			yield return null;
		}
		transform.localPosition = originalPosition;
	}

}
