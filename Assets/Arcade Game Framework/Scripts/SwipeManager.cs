﻿using UnityEngine;

public enum Swipe { None, Tap, Up, Down, Left, Right };

public class SwipeManager : Singleton<SwipeManager>
{
	[SerializeField]
	private float minSwipeLength = 5f;
	private float tapLength = 5f;
	private float swipeNoiseMin = -0.5f;
	private float swipeNoiseMax = 0.5f;

	private Vector2 firstPressPos;
	private Vector2 secondPressPos;
	private Vector2 currentSwipe;

	private Vector2 firstClickPos;
	private Vector2 secondClickPos;

	public static Swipe swipeDirection;

	private bool hasSwipeDetected;

	void Update ()
	{
		if (GameManager.Instance.currentGameState == GameManager.GameState.Running)
		{
			DetectSwipe ();
		}
	}

	public void DetectSwipe ()
	{
		if (Input.touches.Length > 0)
		{
			Touch t = Input.GetTouch (0);

			if (t.phase == TouchPhase.Began)
			{
				firstPressPos = new Vector2 (t.position.x, t.position.y);
				hasSwipeDetected = false;
			}

			secondPressPos = new Vector2 (t.position.x, t.position.y);
			currentSwipe = new Vector3 (secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

			if (t.phase == TouchPhase.Moved && hasSwipeDetected == false && currentSwipe.magnitude > minSwipeLength)
			{
				calculateSwipe ();
			}

			if (t.phase == TouchPhase.Ended && hasSwipeDetected == false)
			{
				// Make sure it was a legit swipe, not a tap
				if (currentSwipe.magnitude <= tapLength)
				{
					swipeDirection = Swipe.Tap;
					return;
				}

				if (currentSwipe.magnitude > minSwipeLength)
				{
					calculateSwipe ();
				}
			}
		}
		else
		{

			if (Input.GetMouseButtonDown (0))
			{
				firstClickPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
				hasSwipeDetected = false;
			}
			else
			{
				resetSwipe ();
			}
			if (Input.GetMouseButtonUp (0))
			{
				secondClickPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
				currentSwipe = new Vector3 (secondClickPos.x - firstClickPos.x, secondClickPos.y - firstClickPos.y);

				// Make sure it was a legit swipe, not a tap
				if (currentSwipe.magnitude <= tapLength)
				{
					swipeDirection = Swipe.Tap;
					return;
				}

				calculateSwipe ();
			}
		}
	}

	private void calculateSwipe()
	{
		if (hasSwipeDetected == false)
		{
			currentSwipe.Normalize ();

			// Swipe up
			if (currentSwipe.y > 0 && currentSwipe.x > swipeNoiseMin && currentSwipe.x < swipeNoiseMax)
			{
				swipeDirection = Swipe.Up;
				hasSwipeDetected = true;
			}
			else if (currentSwipe.y < 0 && currentSwipe.x > swipeNoiseMin && currentSwipe.x < swipeNoiseMax)
			{
				swipeDirection = Swipe.Down;
				hasSwipeDetected = true;
			}
			else if (currentSwipe.x < 0 && currentSwipe.y > swipeNoiseMin && currentSwipe.y < swipeNoiseMax)
			{
				swipeDirection = Swipe.Left;
				hasSwipeDetected = true;
			}
			else if (currentSwipe.x > 0 && currentSwipe.y > swipeNoiseMin && currentSwipe.y < swipeNoiseMax)
			{
				swipeDirection = Swipe.Right;
				hasSwipeDetected = true;
			}
		}
	}

	public void resetSwipe ()
	{
		swipeDirection = Swipe.None;
	}
}