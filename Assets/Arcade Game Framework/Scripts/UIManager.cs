﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour 
{
	public GameObject mainPanel;
	public GameObject gameplayPanel;
	public GameObject pausePanel;
	public GameObject gameOverPanel;
	public GameObject characterSelectPanel;
	public GameObject gameOverContinuePanel;
	public GameObject unlockAvailableButton;
	public Text scoreText;
	public Text coinText;
	public Text gameOverScoreText;
	public Text gameOverBestScoreText;
	public Text continueCoinText;
	public Text currentCharacterNameText;
	public Button soundButton;
	public Button muteButton;

	/*UI events*/
	public delegate void UIEvent();

	public static event UIEvent onCharacterSelectMenuEvent;
	public static event UIEvent onCharacterSelectMenuExitEvent;
	/**/
	void OnEnable()
	{
		GameManager.onGameReadyEvent += onGameReadyEvent;
		GameManager.onGameStartedEvent += onGameStartedEvent;
		GameManager.onGameRunningEvent += onGameRunningEvent;
		GameManager.onGamePausedEvent += onGamePausedEvent;
		GameManager.onGameResumedEvent += onGameResumedEvent;
		GameManager.onGameContinueEvent += onGameContinueEvent;
		GameManager.onGameOverEvent += onGameOverEvent;
		GameManager.onBestScoreEvent += onBestScoreEvent;

		GameManager.onGameRestartEvent += onGameRestartEvent;
		GameManager.onAddCoinEvent += onAddCoinEvent;
		GameManager.onAddScoreEvent += onAddScoreEvent;
	}

	void OnDisable()
	{
		GameManager.onGameReadyEvent -= onGameReadyEvent;
		GameManager.onGameStartedEvent -= onGameStartedEvent;
		GameManager.onGameRunningEvent -= onGameRunningEvent;
		GameManager.onGamePausedEvent -= onGamePausedEvent;
		GameManager.onGameResumedEvent -= onGameResumedEvent;
		GameManager.onGameContinueEvent -= onGameContinueEvent;
		GameManager.onGameOverEvent -= onGameOverEvent;
		GameManager.onBestScoreEvent -= onBestScoreEvent;

		GameManager.onGameRestartEvent -= onGameRestartEvent;
		GameManager.onAddCoinEvent -= onAddCoinEvent;
		GameManager.onAddScoreEvent -= onAddScoreEvent;
	}

	void Start () 
	{
		initialize ();
	}

	void Update()
	{
		updateCoinUI ();
		updateScoreUI ();
	}

	void initialize()
	{
		scoreText.text = "0";
		coinText.text = GameManager.Instance.getCoin ().ToString ();

		mainPanel.SetActive (true);
		gameplayPanel.SetActive (false);
		pausePanel.SetActive (false);
		gameOverPanel.SetActive (false);
		characterSelectPanel.SetActive (false);
		updateSoundUI ();
	}

	public void updateCoinUI ()
	{
		coinText.text = GameManager.Instance.getCoin().ToString ();
	}

	public void updateScoreUI ()
	{
		scoreText.text = GameManager.Instance.getScore().ToString ();
	}

	public void updateSoundUI()
	{
		if (AudioListener.volume > 0)
		{
			soundButton.gameObject.SetActive (true);
			muteButton.gameObject.SetActive (false);
		} 
		else
		{
			soundButton.gameObject.SetActive (false);
			muteButton.gameObject.SetActive (true);
		}
	}

	public void onClickSoundButton()
	{
		AudioListener.volume = 0;
		updateSoundUI ();
		GameManager.Instance.SaveData ();
	}

	public void onClickMuteButton()
	{
		AudioListener.volume = 1;
		updateSoundUI ();
		GameManager.Instance.SaveData ();
	}

	/* Events */

	public void onCharacterSelectMenuAction ()
	{
		mainPanel.SetActive (false);
		gameOverPanel.SetActive (false);
		characterSelectPanel.SetActive (true);

		if (onCharacterSelectMenuEvent != null)
		{
			onCharacterSelectMenuEvent ();
		}
	}

	public void onCharacterSelectMenuExitAction ()
	{
		characterSelectPanel.SetActive (false);
		mainPanel.SetActive (true);

		if (onCharacterSelectMenuExitEvent != null)
		{
			onCharacterSelectMenuExitEvent ();
		}
	}

	public void onHomeEvent()
	{
		scoreText.text = "0";
	}

	public void onGameReadyEvent()
	{
		initialize ();
	}

	public void onGameStartedEvent ()
	{
		currentCharacterNameText.text = GameManager.Instance.getCurrentCharacterName ();
		mainPanel.SetActive (false);
		gameplayPanel.SetActive (true);
	}

	public void onGameRunningEvent ()
	{
	}

	public void onGamePausedEvent()
	{
		pausePanel.SetActive (true);
	}

	public void onGameResumedEvent()
	{
		gameplayPanel.SetActive (true);
		pausePanel.SetActive (false);
	}

	public void onGameContinueEvent()
	{
		gameplayPanel.SetActive (true);
		gameOverPanel.SetActive (false);
	}

	public void onGameOverEvent()
	{		
		updateGameOverScoreUI ();
		updateGameOverBestScoreUI ();
		updateContinueCoinUI ();
		updateCharacterUnlockAvailableUI ();

		StartCoroutine (onGameOverAction (GameManager.Instance.gameOverDelayAfterCrash));
	}

	public void onGameRestartEvent ()
	{
		scoreText.text = "0";
		gameplayPanel.SetActive (true);
		gameOverPanel.SetActive (false);
	}

	public void onAddCoinEvent ()
	{
		coinText.text = GameManager.Instance.getCoin ().ToString ();
	}

	public void onAddScoreEvent ()
	{
		scoreText.text = GameManager.Instance.getScore ().ToString ();
	}

	public void onTapEvent()
	{
	}

	public void onBestScoreEvent()
	{
		updateGameOverBestScoreUI ();
	}

	IEnumerator onGameOverAction(float time)
	{
		yield return new WaitForSeconds(time);

		gameOverPanel.SetActive (true);
	}

	private void updateGameOverScoreUI()
	{
		gameOverScoreText.text = GameManager.Instance.getScore ().ToString ();
	}

	private void updateGameOverBestScoreUI()
	{
		gameOverBestScoreText.text = GameManager.Instance.getBestScore ().ToString ();
	}

	public void updateContinueCoinUI()
	{
		int coin = GameManager.Instance.getCoin ();
		int cointinueLifeCost = GameManager.Instance.getContinueLifeCost ();

		if (coin >= cointinueLifeCost)
		{
			continueCoinText.text = cointinueLifeCost.ToString();
			gameOverContinuePanel.SetActive (true);
		} 
		else
		{
			gameOverContinuePanel.SetActive (false);
		}
	}

	private void updateCharacterUnlockAvailableUI ()
	{
		if (DataClass.Instance.checkAvailableCharacterForUnlock (GameManager.Instance.getCoin ()))
		{
			unlockAvailableButton.SetActive (true);
		}
	}

	public void OnClickCharacterSelectButton ()
	{
		onCharacterSelectMenuAction ();
	}

	public void OnClickCharacterSelectCloseButton ()
	{
		onCharacterSelectMenuExitAction ();
	}

	public void onClickBuyCharacterButton ()
	{
		unlockAvailableButton.SetActive (false);
		onCharacterSelectMenuAction ();
	}
}
