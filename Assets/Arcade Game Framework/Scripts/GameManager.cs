﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameManager : Singleton<GameManager> 
{
	[SerializeField]
	private CharacterData currentCharacter;

	[SerializeField]
	private GameObject playerPrefab;

	[SerializeField]
	private GameObject enemyPrefab;

	[SerializeField]
	private GameObject groundPrefab;

	[SerializeField]
	private GameObject coinPrefab;

	[SerializeField]
	public float objectSpawnXDistance = 2;

	[SerializeField]
	public float gameOverDelayAfterCrash = 1f;

	[SerializeField]
	public float gameOverAfterFallingHeight = 5f;

	[SerializeField]
	public int continueLifeCost = 500;

	[SerializeField]
	public int gameOverCounter = 0;

	[SerializeField]
	private AudioSource musicSource;

	[SerializeField, Range(0f, 1f)]
	private float menuMusicVolume = 0.25f;

	[SerializeField, Range(0f, 1f)]
	private float gameMusicVolume = 1f;

	[SerializeField]
	private AudioClip coinAudioClip;

	[SerializeField]
	private AudioClip coinSpentAudioClip;

	[SerializeField]
	public AudioClip collisionAudioClip;

	[SerializeField]
	public AudioClip movementAudioClip;

	[SerializeField]
	public AudioClip jumpAudioClip;

	[SerializeField]
	private bool resetDataOnStart = false;

	[HideInInspector]
	public enum GameState 
	{
		NotReady,
		Ready,
		Running,
		Paused,
		GameOver
	}

	[HideInInspector]
	public GameState currentGameState;

	private bool adsEnabled;
	private int coin = 0;
	private int score = 0;
	private int bestScore = 0;
	private string DATA_FILE_PATH = "/data.db";

	/* Events */
	public delegate void Event();
	public static event Event onGameReadyEvent;
	public static event Event onGameStartedEvent;
	public static event Event onGameRunningEvent;
	public static event Event onGamePausedEvent;
	public static event Event onGameResumedEvent;
	public static event Event onGameContinueEvent;
	public static event Event onGameOverEvent;
	public static event Event onBestScoreEvent;

	public static event Event onGameRestartEvent;
	public static event Event onAddCoinEvent;
	public static event Event onAddScoreEvent;

	/* Actions */
	private void onGameReadyAction()
	{
		gameReadyInitialize ();

		if (onGameReadyEvent != null)
		{
			onGameReadyEvent ();
		}
	}

	private void onGameStartedAction()
	{
		gameStartInitialize ();

		if (onGameStartedEvent != null)
		{
			onGameStartedEvent ();
		}
	}

	private void onGameRunningAction()
	{
		onGameRunningEvent ();

		if (onGameRunningEvent != null)
		{
			onGameRunningEvent ();
		}
	}

	private void onGamePausedAction()
	{
		currentGameState = GameState.Paused;
		musicSource.volume = menuMusicVolume;

		if (onGamePausedEvent != null)
		{
			onGamePausedEvent ();
		}
	}

	private void onGameResumedAction()
	{
		currentGameState = GameState.Running;
		musicSource.volume = gameMusicVolume;

		if (onGameResumedEvent != null)
		{
			onGameResumedEvent ();
		}
	}

	private void onGameContinueAction()
	{
		if (coin >= continueLifeCost)
		{
			gameStartInitialize ();

			coin -= continueLifeCost;

			if (coin < 0)
			{
				coin = 0;
			}

			if (onGameContinueEvent != null)
			{
				onGameContinueEvent ();
			}

			playAudioClip (coinSpentAudioClip, 10f);
		}
	}

	public void onGameRestartAction ()
	{
		if (onGameRestartEvent != null)
		{
			onGameRestartEvent ();
		}

		SaveData ();
		score = 0;
	}

	public void onGameOverAction()
	{
		++gameOverCounter;
		currentGameState = GameState.GameOver;
		musicSource.volume = menuMusicVolume;

		if (onGameOverEvent != null)
		{
			onGameOverEvent ();
		}

		SaveData ();
	}

	private void onUICharactersPanelExit ()
	{
		SaveData ();
		onGameReadyAction ();
	}

	private void onAddCoinAction ()
	{
		addCoin ();

		if (onAddCoinEvent != null)
		{
			onAddCoinEvent ();
		}
	}

	private void onAddScoreAction ()
	{
		addScore ();
		checkForBestScore ();

		if (onAddScoreEvent != null)
		{
			onAddScoreEvent ();
		}
	}

	private void onBestScoreAction()
	{
		bestScore = score;

		if (onBestScoreEvent != null)
		{
			onBestScoreEvent ();
		}
	}

	/* Buttons */

	public void onClickGameOverButton()
	{
		onGameOverAction ();
	}

	public void onClickPlayButton()
	{
		onGameStartedAction ();
	}

	public void onClickPauseButton()
	{
		onGamePausedAction ();
	}

	public void onClickUndoButton()
	{
	}

	public void onClickHomeButton()
	{
		SaveData ();
		onGameReadyAction ();
	}

	public void onClickResumeButton()
	{
		onGameResumedAction ();
	}

	public void onClickContinueButton()
	{
		onGameContinueAction ();
	}

	public void onClickRestartButton ()
	{
		onGameRestartAction ();
	}

	public void onClickCoinButton ()
	{
		onAddCoinAction ();
	}

	public void onClickScoreButton ()
	{
		onAddScoreAction ();
	}

	/* Unity Callbacks */

	void Start () 
	{
		currentGameState = GameState.NotReady;
		LoadData ();
		onGameReadyAction ();
		setSelectedCharacter ();

		PlayerPrefs.DeleteKey ("FirstLaunch"); // test: ads will be re-enabled each application start

		if (!PlayerPrefs.HasKey ("FirstLaunch"))
		{
			PlayerPrefs.SetInt ("FirstLaunch", 0);
			PlayerPrefs.Save ();
			adsEnabled = true;
			SaveData ();
		}

		if (adsEnabled)
		{
			AdManager.Instance.Initialize ();
		}
		else
		{
			AdManager.Instance.turnOffAds ();
		}
	}

	void OnEnable ()
	{
		UICharacterController.onCharacterSelect += setSelectedCharacter;
		UIManager.onCharacterSelectMenuExitEvent += onUICharactersPanelExit;
	}

	void OnDisable ()
	{
		UICharacterController.onCharacterSelect -= setSelectedCharacter;
		UIManager.onCharacterSelectMenuExitEvent -= onUICharactersPanelExit;
	}
	/**/

	void gameReadyInitialize()
	{
		currentGameState = GameState.Ready;
		musicSource.volume = menuMusicVolume;
		score = 0;
	}

	void gameStartInitialize()
	{
		currentGameState = GameState.Running;
		musicSource.volume = gameMusicVolume;
		playAudioClip (jumpAudioClip, 1f);
	}

	public int getBestScore()
	{
		return bestScore;
	}

	public void checkForBestScore()
	{
		if (score > bestScore)
		{
			onBestScoreAction ();
		}
	}

	public bool makePurchase (int price)
	{
		if (coin - price >= 0)
		{
			coin -= price;
			SaveData ();
			return true;
		}

		return false;
	}

	public int getContinueLifeCost()
	{
		return continueLifeCost;
	}

	public void addScore()
	{
		score++;
	}

	public void addCoin()
	{
		coin++;
		playAudioClip (coinAudioClip, 0.5f);
	}

	public int getScore()
	{
		return score;
	}

	public int getCoin()
	{
		return coin;
	}

	public int getGameOverCount ()
	{
		return gameOverCounter;
	}

	public void SaveData()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (getDataFilePath ());

		PlayerData data = new PlayerData ();
		data.adsEnabled = adsEnabled;
		data.bestScore = bestScore;
		data.totalCoins = coin;
		data.soundVolume = AudioListener.volume;

		bf.Serialize (file, data);
		file.Close ();
	}

	public void LoadData()
	{
		ResetData ();

		if (File.Exists (getDataFilePath ()))
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (getDataFilePath (), FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize (file);
			file.Close ();

			adsEnabled = data.adsEnabled;
			bestScore = data.bestScore;
			coin = data.totalCoins;
			AudioListener.volume = data.soundVolume;
		}

		SaveData ();
	}

	public void ResetData()
	{
		if (resetDataOnStart && File.Exists (getDataFilePath ()))
		{
			File.Delete (getDataFilePath ());
		}
	}

	public string getDataFilePath()
	{
		return Application.persistentDataPath + DATA_FILE_PATH;
	}

	public void playAudioClip(AudioClip audioClip, float volume)
	{
		AudioSource.PlayClipAtPoint (audioClip, playerPrefab.transform.position, volume);
	}

	public Vector3 getPlayerCurrentPosition()
	{
		return playerPrefab.transform.position;
	}

	public string getCurrentCharacterName ()
	{
		if (currentCharacter == null)
		{
			return "";
		}

		return currentCharacter.name;
	}

	public void setSelectedCharacter ()
	{
		currentCharacter = DataClass.Instance.getCurrentCharacter ();
	}

	public void disableAds ()
	{
		adsEnabled = false;
		AdManager.Instance.turnOffAds ();
		SaveData ();
	}
}
