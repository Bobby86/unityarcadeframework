﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour 
{
	[SerializeField]
	private Vector3 spawnPosition = new Vector3(0f, 0.5f, 0f);

	[SerializeField]
	private float groundYPosition = 0.5f;

	[SerializeField]
	private float forwardSpeed = 10f;

	[SerializeField]
	private float sidewaysSpeed = 10f;

	[SerializeField]
	private float jumpPower = 10f;

	private bool shouldMove = false;
	private int totalJump = 2;
	private int currentJump = 1;
	private float horizontalMoveSteps;
	private bool isMoveUpEventTriggered = false;
	private float targetXPosition = 0f;
	private float respawnZPosition = -30;

	private Rigidbody rb;

	/* Events */
	public delegate void Event();

	void OnEnable()
	{
		GameManager.onGameReadyEvent += onGameReadyEvent;
		GameManager.onGameStartedEvent += onGameStartedEvent;
		GameManager.onGameOverEvent += onGameOverEvent;
		GameManager.onGamePausedEvent += onGamePausedEvent;
		GameManager.onGameResumedEvent += onGameResumedEvent;
		GameManager.onGameContinueEvent += onGameContinueEvent;
	}

	void OnDisable()
	{
		GameManager.onGameReadyEvent -= onGameReadyEvent;
		GameManager.onGameStartedEvent -= onGameStartedEvent;
		GameManager.onGameOverEvent -= onGameOverEvent;
		GameManager.onGamePausedEvent -= onGamePausedEvent;
		GameManager.onGameResumedEvent -= onGameResumedEvent;
		GameManager.onGameContinueEvent -= onGameContinueEvent;
	}

	public void onGameReadyEvent()
	{
		initialize ();
	}

	public void onGameStartedEvent()
	{		
		shouldMove = true;
		rb.isKinematic = false;
	}

	public void onGameOverEvent()
	{
		shouldMove = false;
		rb.isKinematic = true;
	}

	public void onGamePausedEvent()
	{
		shouldMove = false;
	}

	public void onGameResumedEvent()
	{
		shouldMove = true;
	}

	public void onGameContinueEvent()
	{
		Vector3 pos = spawnPosition;
		pos.z = transform.position.z + respawnZPosition;

		transform.position = pos;
		shouldMove = true;
		rb.isKinematic = false;
		isMoveUpEventTriggered = false;
		targetXPosition = 0f;
	}

	public void onTapAction()
	{
		if (shouldMove)
		{
			//Debug.Log ("Tap");
		}
	}

	private void onMoveUpAction()
	{
		if (shouldMove)
		{
			if (isMoveUpEventTriggered == false && currentJump < totalJump)
			{
				isMoveUpEventTriggered = true;
			}
		}
	}

	private void onMoveDownAction()
	{
		if (shouldMove)
		{
			//Debug.Log ("Down");
		}
	}

	private void onMoveLeftAction()
	{
		if (shouldMove)
		{
			targetXPosition -= horizontalMoveSteps;
			GameManager.Instance.playAudioClip (GameManager.Instance.movementAudioClip, 0.8f);
		}
	}

	private void onMoveRightAction()
	{
		if (shouldMove)
		{
			targetXPosition += horizontalMoveSteps;
			GameManager.Instance.playAudioClip (GameManager.Instance.movementAudioClip, 0.8f);
		}
	}

	void Start () 
	{
		rb = GetComponent<Rigidbody>();
		StartCoroutine (moveForward ());
		StartCoroutine (moveSideways ());
		StartCoroutine (jump ());
		StartCoroutine (checkForFalling ());
	}

	void Update ()
	{
		inputControl ();
	}

	private void initialize()
	{
		rb.isKinematic = true;
		horizontalMoveSteps = GameManager.Instance.objectSpawnXDistance;
		isMoveUpEventTriggered = false;
		transform.position = spawnPosition;
		targetXPosition = 0f;
	}

	private IEnumerator moveForward()
	{
		while (true)
		{
			if (shouldMove)
			{
				Vector3 pos = transform.position;
				pos.z += Time.deltaTime * forwardSpeed;

				transform.position = pos;
				transform.rotation = Quaternion.identity;
			}

			yield return 0;
		}
	}

	private IEnumerator moveSideways()
	{
		while (true)
		{
			if (shouldMove)
			{
				Vector3 pos = transform.position;
				pos.x = targetXPosition;

				transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * sidewaysSpeed);
			}

			yield return 0;
		}
	}

	private IEnumerator jump()
	{
		while (true)
		{
			if (shouldMove)
			{
				if (isMoveUpEventTriggered && currentJump < totalJump)
				{
					rb.AddForce(transform.up * jumpPower);
					isMoveUpEventTriggered = false;
					currentJump++;
					GameManager.Instance.playAudioClip (GameManager.Instance.jumpAudioClip, 1f);
				}

				resetTotalJump ();
			}

			yield return 0;
		}
	}

	private IEnumerator checkForFalling()
	{
		while (true)
		{
			if (shouldMove == true && 
				GameManager.Instance.currentGameState == GameManager.GameState.Running && 
				transform.position.y < groundYPosition - GameManager.Instance.gameOverAfterFallingHeight)
			{
				GameManager.Instance.playAudioClip (GameManager.Instance.collisionAudioClip, 1f);	
				GameManager.Instance.onGameOverAction ();
			}

			yield return 0;
		}
	}

	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Enemy" || collider.gameObject.tag == "Obstacle")
		{
			shouldMove = false;
			GameManager.Instance.playAudioClip (GameManager.Instance.collisionAudioClip, 1f);	
			GameManager.Instance.onGameOverAction ();
		}
	}

	private void resetTotalJump()
	{
		if (transform.position.y <= groundYPosition)
		{
			currentJump = 1;
		}
	}

	void inputControl()
	{
		if (shouldMove && GameManager.Instance.currentGameState == GameManager.GameState.Running)
		{
			if (SwipeManager.swipeDirection == Swipe.Tap)
			{
				onTapAction ();
				SwipeManager.Instance.resetSwipe ();
			}
			else if (Input.GetKeyDown ("up") || SwipeManager.swipeDirection == Swipe.Up)
			{
				onMoveUpAction ();
				SwipeManager.Instance.resetSwipe ();
			}
			else if (Input.GetKeyDown ("down") || SwipeManager.swipeDirection == Swipe.Down)
			{
				onMoveDownAction ();
				SwipeManager.Instance.resetSwipe ();
			}
			else if (Input.GetKeyDown ("left") || SwipeManager.swipeDirection == Swipe.Left)
			{
				onMoveLeftAction ();
				SwipeManager.Instance.resetSwipe ();
			}
			else if (Input.GetKeyDown ("right") || SwipeManager.swipeDirection == Swipe.Right)
			{
				onMoveRightAction ();
				SwipeManager.Instance.resetSwipe ();
			}
		}
	}
}
