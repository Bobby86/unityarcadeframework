﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour {	

	[SerializeField]
	private GameObject[] prefabs = new GameObject[1];

	[SerializeField, Range(0, 100)]
	private int samePrefabSpawnRate = 0;

	[SerializeField]
	private int maxPrefabsOnScreen = 10;

	[SerializeField]
	private int totalPrefabsPerWave = 10;

	[SerializeField]
	private int distanceBetweenWaves = 500;

	[SerializeField]
	private Vector3 firstSpawnPosition = new Vector3(0, 0, 10);

	[SerializeField, Range(1, 10)]
	private int maxXSpawnPositions = 1;

	[SerializeField, Range(0, 100)]
	private int sameXLineSpawnRate = 0;

	//[SerializeField, Range(0, 100)]
	//private int sameZLineSpawnRate = 0;

	[SerializeField]
	public Vector3 spawnDistance = new Vector3 (3, 0, 1);

	[SerializeField, Range(0, 100)]
	private int yDistanceMultiplierRate = 0;

	[SerializeField, Range(0, 100)]
	private int zDistanceMultiplierRate = 5;



	private List<GameObject> prefabList = new List<GameObject> ();
	private int lastPrefabIndex = 0;
	private float lastXPosition = 0;
	private int totalSpawned = 0;
	private Vector3 lastObjPosition;

	void OnEnable()
	{
		GameManager.onGameReadyEvent += onGameReadyEvent;
	}

	void OnDisable()
	{
		GameManager.onGameReadyEvent -= onGameReadyEvent;
	}

	public void onGameReadyEvent()
	{
		foreach(GameObject go in prefabList)
		{
			Destroy (go);
		}
		prefabList.RemoveAll( obj => obj == null);
		prefabList = new List<GameObject> ();

		lastPrefabIndex = UnityEngine.Random.Range (0, prefabs.Length);
		totalSpawned = 0;
	}

	void Start () 
	{
		StartCoroutine(spawnerCoroutine ());
	}

	private IEnumerator spawnerCoroutine()
	{
		int count;
		int prefabIndex;

		while (true)
		{
			count = getObjectCount (prefabList);
			if ( count < maxPrefabsOnScreen)
			{
				prefabIndex = lastPrefabIndex;

				/*
				 * samePrefabSpawnRate:
				 * 0 => never same prefab
				 * 100 => always same prefab, never enter following block
				 */
				if (UnityEngine.Random.Range (0, 100) >= samePrefabSpawnRate)
				{
					prefabIndex = UnityEngine.Random.Range (0, prefabs.Length);
					lastPrefabIndex = prefabIndex;
				}

				if (totalSpawned < totalPrefabsPerWave)
				{
					spawnObject (prefabs [prefabIndex]);
				} 
				else if(GameManager.Instance.getPlayerCurrentPosition().z > lastObjPosition.z + distanceBetweenWaves)
				{
					totalSpawned = 0;
					lastObjPosition.z = GameManager.Instance.getPlayerCurrentPosition ().z;
					if (prefabList.Count > 0 && prefabList [prefabList.Count - 1] != null)
					{
						Vector3 pos = prefabList [prefabList.Count - 1].transform.position;
						pos.z = GameManager.Instance.getPlayerCurrentPosition ().z + distanceBetweenWaves;
						prefabList [prefabList.Count - 1].transform.position = pos;
					}
				}
			} 
			else
			{
				yield return null;
			}

			yield return 0;
		}
	} 

	private GameObject spawnObject( GameObject prefab )
	{
		Vector3 position = firstSpawnPosition;
		lastObjPosition = getLastObjectPosition ();

		if (maxXSpawnPositions > 1)
		{
			position.x = lastXPosition;

			/*
		 * sameXLineSpawnRate:
		 * 0 => never same line
		 * 100 => always same line, never enter following block
		 */
			if (UnityEngine.Random.Range (0, 100) >= sameXLineSpawnRate)
			{
				position.x = spawnDistance.x * UnityEngine.Random.Range (0, maxXSpawnPositions) - spawnDistance.x * (maxXSpawnPositions - 1) / 2;
				lastXPosition = position.x;
			}
		}

		if (yDistanceMultiplierRate > 0)
		{
			position.y = lastObjPosition.y + spawnDistance.y * UnityEngine.Random.Range (1, yDistanceMultiplierRate + 1);
		} else
		{
			position.y = lastObjPosition.y;
		}

		if (zDistanceMultiplierRate > 0)
		{
			position.z = lastObjPosition.z + spawnDistance.z * UnityEngine.Random.Range (1, zDistanceMultiplierRate + 1);
		} else
		{
			position.z = lastObjPosition.z;
		}

		if (checkIfPosEmpty (position))
		{

			GameObject obj = Instantiate (prefab) as GameObject;
			obj.transform.position = position;

			prefabList.Add (obj);
			totalSpawned++;

			return obj;
		} else
		{
			Debug.Log ("Overlap Found");
		}

		return null;
	}

	private int getObjectCount(List<GameObject> objList)
	{
		return objList.FindAll (obj => obj != null).Count;
	}

	private Vector3 getLastObjectPosition()
	{
		Vector3 lastObjPosition = Vector3.zero;
		if (prefabList.Count > 0 && prefabList [prefabList.Count - 1] != null)
		{
			lastObjPosition = prefabList [prefabList.Count - 1].transform.position;
		} 
		else
		{
			lastObjPosition = firstSpawnPosition;
			lastObjPosition.z = GameManager.Instance.getPlayerCurrentPosition ().z + firstSpawnPosition.z;
		}

		return lastObjPosition;
	}

	public bool checkIfPosEmpty(Vector3 targetPos)
	{
		GameObject[] allObstacles = GameObject.FindGameObjectsWithTag("Obstacle");
		GameObject[] allCollectables = GameObject.FindGameObjectsWithTag("Collectable");

		foreach(GameObject current in allObstacles)
		{
			//if (current.transform.position == targetPos)
			if(current.GetComponentInChildren<Renderer>().bounds.Contains(targetPos))
			{
				return false;
			}
		}

		foreach(GameObject current in allCollectables)
		{
			if(current.GetComponentInChildren<Renderer>().bounds.Contains(targetPos))
			{
				return false;
			}
		}

		return true;
	}
}
