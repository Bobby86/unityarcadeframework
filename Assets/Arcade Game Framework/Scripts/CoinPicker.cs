﻿using UnityEngine;
using System.Collections;

public class CoinPicker : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Player")
		{
			Pickup ();
		}
	}

	private void Pickup()
	{
		GameManager.Instance.addCoin();
		Destroy (gameObject.transform.parent.gameObject);
	}
}
